<?php $this->extend('Bootstrap./Common/form'); ?>

<?php $this->assign('pageHeader', 'Instâncias'); ?>


<?php $this->start('form-create');?>
	<?php echo $this->Bootstrap->create('Instancia'); ?>
<?php $this->end(); ?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->actions(null, $formActions); ?>
<?php $this->end(); ?>

<?php $this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('instance', array('label'=>'Instância'));
	echo $this->Bootstrap->input('versao_cake');
	echo $this->Bootstrap->input('versao_php');
	echo $this->Bootstrap->input('salt');
	echo $this->Bootstrap->input('cipherseed');
	echo $this->Bootstrap->input('logo');
$this->end(); ?>

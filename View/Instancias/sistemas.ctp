<?php $this->extend('Bootstrap./Common/index'); ?>

<?php $this->assign('panelStyle','primary'); ?>
<?php $this->assign('pageHeader','Sistemas da Instância'); ?>


<?php $this->start('actions'); ?>
	<?php echo $this->Bootstrap->actions(null, $listActions); ?>
<?php $this->end(); ?>

<<?php $this->start('table-tr'); ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th class="col-md-10">Sistema</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
	<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(
			null,
			array('action'=>'edit_sistema', $Model['id'], $Model['instancia_id']),
			array(
				'icon'=>'pencil',
				'title' => 'Editar'
			)
			); ?>
			<?php echo $this->Bootstrap->btnLink(
			null,
			array('action'=>'del_sistema', $Model['id'], $Model['instancia_id']),
			array(
				'icon'=>'trash',
				'title' => 'Excluir',
				'method'=>'post',
				'confirm'=>true,
				'message'=>'Tem Certeza?'
			)
			); ?>
			</div>

		</td>
		<td><?php echo $Model['Sistema']['nome']; ?></td>
	</tr>
	<?php } ?>
<?php $this->end(); ?>


<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('email');
	echo $this->Bootstrap->input('grupo_id',array('label'=>'Grupo','options'=>$Grupos));
	echo '<div class="panel panel-default"><div class="panel-heading">Senha</div><div class="panel-body">';
	echo $this->Bootstrap->input('senha1',array('type'=>'password','label'=>'Senha'));
	echo $this->Bootstrap->input('senha2',array('type'=>'password','label'=>'Confirmação'));
	echo '</div></div>';$this->end();
?>

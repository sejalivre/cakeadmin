<?php $this->extend('Bootstrap./Common/login'); ?>

<?php $this->assign('panelStyle','success'); ?>
<?php $this->assign('pageHeader','Login'); ?>

<?php $this->start('table-body'); ?>
	<div class="row">
	<div class="col-md-4">&nbsp;</div>
	<div class="col-md-4">
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Autenticação</h3>
			</div>
			<div class="panel-body" >
				
				<?php
				echo $this->Bootstrap->create('Usuario');
				echo $this->Bootstrap->input('email',array('label'=>'E-mail'));
				echo $this->Bootstrap->input('senha',array('label'=>'Senha','type'=>'password','value'=>''));
				echo $this->Bootstrap->submit('Fazer Login',array('class'=>'btn btn-primary '));
				echo $this->Bootstrap->end();
				if (isset($pass)) echo $pass;
				?>
			</div>
			<div class="panel-footer clearfix">
				<span class="pull-right"><?php echo $this->Html->link('Esqueceu sua senha ?', array('action'=>'password')); ?></span>
			</div>
		</div>
		
	</div>
	<div class="col-md-4">&nbsp;</div>
</div>
<?php $this->end(); ?>

<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader','Links do Menu'); // Header da página
	$this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions'); ?>
		<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'linksAdd', $menu_id, $grupo_id)); ?>
		<?php echo $this->Bootstrap->btnLink('Voltar', array('action'=>'index')); ?>
<?php $this->end(); ?>

<?php $this->start('bofore-table'); ?>
<div class="navbar navbar-default">
		<ul class="nav navbar-nav">
			<?php foreach($Links as $link) { ?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $link['Link']['texto'];?><b class="caret"></b></a>
				<ul class="dropdown-menu">
					<?php if (!empty($link['children'])) { ?>
					<?php foreach($link['children'] as $sublink ) { ?>
					<li>
						<a href="#"><?php echo $sublink['Link']['texto'];?></a>
					</li>
					<?php } } ?>
				</ul>
			</li>
			<?php } ?>
		</ul>
	</div>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th>Link</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Link) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(
			null,
			array('action'=>'linksEdit', $Link['Link']['menu_id'], $Link['Menu']['grupo_id'], $Link['Link']['id']),
			array(
				'icon' => 'pencil',
				'title' => 'Editar',
				'size' => 'btn-sm'
			)
			); ?>
			<?php echo $this->Bootstrap->btnLink(
			null,
			array('action'=>'linksDel', $Link['Link']['id']),
			array(
			'icon' => 'trash',
			'title' => 'Excluir',
			'size' => 'btn-sm'
				)
			); ?>
			</div>
		</td>
		<td><?php echo $Link['Link']['texto']; ?></td>
	</tr>
	<?php if (!empty($Link['children'])) { ?>
		<?php foreach($Link['children'] as $sublink ) { ?>
			<tr>
				<td>
					<div class="btn-group">
					<?php echo $this->Bootstrap->btnLink(
					null,
					array('action'=>'linksEdit', $sublink['Link']['menu_id'], $sublink['Menu']['grupo_id'], $sublink['Link']['id']),
					array(
						'icon' => 'pencil',
						'title' => 'Editar',
						'size' => 'btn-sm'
					)
					); ?>
					<?php echo $this->Bootstrap->btnLink(
					null,
					array('action'=>'delLink', $sublink['Link']['id']),
					array(
						'icon' => 'trash',
						'title' => 'Excluir',
						'size' => 'btn-sm'
					)
					); ?>
					</div>
				</td>
				<td>&nbsp;&nbsp;&nbsp;<?php echo $sublink['Link']['texto'];?></td>
			</tr>
		<?php } ?>
	<?php } ?>
<?php } ?>
<?php $this->end(); ?>


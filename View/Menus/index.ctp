<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'add')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
		<th class="col-md-4">Grupo</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'edit', $Model['Menu']['id']), array('icon'=>'pencil')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'del', $Model['Menu']['id']), array('icon'=>'trash','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'links', $Model['Menu']['id']), array('icon'=>'list-alt')); ?>
			</div>
		</td>
		<td><?php echo $Model['Menu']['nome']; ?></td>
		<td><?php echo $Model['Grupo']['nome']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>

<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'links',$menu_id));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('texto', array('label'=>'Nome'));
	echo $this->Bootstrap->input('permissao_id', array('label'=>'Permissão','options'=>$Permissoes));
	echo $this->Bootstrap->input('parent_id', array('label'=>'Menu Pai', 'options'=>$Links));
$this->end();
?>

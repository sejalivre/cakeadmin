<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
echo $this->Form->input('plugin',array('label'=>'Plugin'));
	echo $this->Form->input('controller',array('label'=>'Controller'));
	echo $this->Form->input('action',array('label'=>'Action'));
	if ($this->action == 'edit') {
	echo '<div class="row">';
		echo '<div class="col-md-6">';
	
			echo '<div class="panel panel-success">';
				echo '<div class="panel-heading"><h3 class="panel-title">Grupos Selecionados</h3></div>';
				echo '<div class="panel-body">';
				if (isset($this->data['GrupoPermissao'])) {
					echo '<ul class="list-group permit-dels">';
					foreach($this->data['GrupoPermissao'] as $GrupoP) { 
						echo '<li data-id="'.$GrupoP['id'].'" class="list-group-item"><span class="glyphicon glyphicon-trash"></span>&nbsp;'.$GrupoP['Grupo']['nome'].'</li>';
					}
					echo '</ul>';
				}
				echo '</div>';
			echo '</div>';
		
		echo '</div>';
		echo '<div class="col-md-6">';
			
			echo '<div class="panel panel-warning">';
				echo '<div class="panel-heading"><h3 class="panel-title">Grupos Disponíveis</h3></div>';
				echo '<div class="panel-body">';
					echo '<ul class="list-group permit-adds">';
					foreach($Grupos as $key=>$value) { 
						echo '<li data-id="'.$key.'" class="list-group-item"><span class="glyphicon glyphicon-plus"></span>&nbsp;'.$value.'</li>';
					}
					echo '</ul>';
				echo '</div>';
			echo '</div>';
	
		echo '</div>';
	echo '</div>'; 
	}
	?>
	
	<script>
		$(document).ready(function(){
			$('.permit-dels li').click(function(){
				me = $(this);
				$.ajax({
					dataType: 'json',
					url: '/admin/Permissoes/delgrupo/'+me.data('id')+'/<?php echo $permissao_id;?>',
					success: function(data){
						$('.permit-dels').html('');
						$.each(data[0], function(index, value) {
							console.log(value);
							$('.permit-dels').append('<li class="list-group-item"><span class="glyphicon glyphicon-trash"></span>&nbsp;'+value.Grupo.nome+'</li>');
						});
						$('.permit-adds').html('');
						$.each(data[1], function(index, value) {
							console.log(value);
							$('.permit-adds').append('<li class="list-group-item"><span class="glyphicon glyphicon-plus"></span>&nbsp;'+value.Grupo.nome+'</li>');
						});
					}
				});
			});
			$('.permit-adds li').click(function(){
				me = $(this);
				$.ajax({
					dataType: 'json',
					url: '/admin/Permissoes/addgrupo/'+me.data('id')+'/<?php echo $permissao_id;?>',
					success: function(data){
						$('.permit-dels').html('');
						$.each(data[0], function(index, value) {
							console.log(value);
							$('.permit-dels').append('<li class="list-group-item"><span class="glyphicon glyphicon-trash"></span>&nbsp;'+value.Grupo.nome+'</li>');
						});
						$('.permit-adds').html('');
						$.each(data[1], function(index, value) {
							console.log(value);
							$('.permit-adds').append('<li class="list-group-item"><span class="glyphicon glyphicon-plus"></span>&nbsp;'+value.Grupo.nome+'</li>');
						});
					}
				});
			});
		});
	</script>
<?php $this->end();?>

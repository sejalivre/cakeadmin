<?php $this->extend('Bootstrap./Common/index'); ?>

<?php $this->assign('panelStyle','primary'); ?>
<?php $this->assign('pageHeader','Sistemas'); ?>


<?php $this->start('actions'); ?>
	<?php echo $this->Bootstrap->actions(null, $listActions); ?>
<?php $this->end(); ?>

<<?php $this->start('table-tr'); ?>
	<tr class="active">
		<th class="col-md-3">&nbsp;</th>
		<th class="col-md-3"><?php echo $this->Paginator->sort('nome','Nome');?></th>
		<th class="col-md-3"><?php echo $this->Paginator->sort('versao_php','Versão PHP');?></th>
		<th class="col-md-3"><?php echo $this->Paginator->sort('versao_cake','Versão Cake');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
	<?php foreach ($data as $Model) { ?>
	<tr>
		<td><?php echo $this->Bootstrap->actions($Model['Sistema']['id'],$indexActions, array('size'=>'sm')); ?></td>
		<td><?php echo $Model['Sistema']['nome']; ?></td>
		<td><?php echo $Model['Sistema']['versao_php']; ?></td>
		<td><?php echo $Model['Sistema']['versao_cake']; ?></td>
	</tr>
	<?php } ?>
<?php $this->end(); ?>


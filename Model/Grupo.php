<?php
App::uses('AppModel', 'Model');
class Grupo extends AdminAppModel {
	public $useDbConfig = 'Admin';
    public $useTable = 'grupos';
	public $displayField = 'nome';
	public $hasMany = array(
		'GrupoPermissao' => array(
			'className' => 'Admin.GrupoPermissao',
			'foreignKey' => 'grupo_id'
		)
	);
	
}
<?php
App::uses('AppModel', 'Model');

class Sistema extends AdminAppModel {
	public $useDbConfig = 'Admin';
	public $useTable = 'sistemas';
	public $displayField = 'nome';
	
	public $hasMany = array(
		'InstanciaSistema' => array(
			'className' => 'Admin.InstanciaSistema',
			'foreignKey' => 'sistema_id'
		),
		'Menu'
	);
	
}

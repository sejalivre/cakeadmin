<?php
App::uses('AppModel', 'Model');
class Instancia extends AdminAppModel {
	public $useDbConfig = 'Admin';
	public $useTable = 'instancias';
	public $displayField = 'nome';
	
	public $hasMany = array(
		'InstanciaSistema' => array(
			'className' => 'Admin.InstanciaSistema',
			'foreignKey' => 'instancia_id'
		)
	);
	
}

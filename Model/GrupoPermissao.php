<?php
App::uses('AppModel', 'Model');
class GrupoPermissao extends AdminAppModel {
	public $useDbConfig = 'Admin';
    public $useTable = 'grupos_permissoes';
	public $belongsTo = array(
		'Grupo' => array(
			'className' => 'Admin.Grupo',
			'foreignKey' => 'grupo_id'
		),
		'Permissao' => array(
			'className' => 'Admin.Permissao',
			'foreignKey' => 'permissao_id'
		)
	);
	
}
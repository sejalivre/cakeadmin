<?php
App::uses('AppModel', 'Model');
class Versao extends AdminAppModel {

	public $useTable = 'versoes';
	public $useDbConfig = 'Admin';
	public $displayField = 'data';

}

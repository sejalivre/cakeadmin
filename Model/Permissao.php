<?php
App::uses('AppModel', 'Model');
class Permissao extends AdminAppModel {
	public $useDbConfig = 'Admin';
    public $useTable = 'permissoes';
	public $hasMany = array(
		'GrupoPermissao' => array(
			'className' => 'Admin.GrupoPermissao',
			'foreignKey' => 'permissao_id'
		)
	);
	
	public $virtualFields = array(
		'item' => 'CONCAT(Permissao.plugin, " - ", Permissao.controller, " - ", Permissao.action)'
	);
	
}
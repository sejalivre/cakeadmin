<?php
App::uses('AppModel', 'Model');
class InstanciaSistema extends AdminAppModel {
	public $useDbConfig = 'Admin';
	public $useTable = 'instancias_sistemas';
	public $belongsTo = array(
		'Sistema' => array(
			'className' => 'Admin.Sistema',
			'foreignKey' => 'sistema_id'
		),
		'Instancia' => array(
			'className' => 'Admin.Instancia',
			'foreignKey' => 'instancia_id'
		)
	);
	
}
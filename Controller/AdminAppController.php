<?php

App::uses('Controller', 'Controller');

class AdminAppController extends AppController {
	
	public $uses = array('Admin.Sistema');
	
	public $components = array(
		'Admin.MenuAdmin'
	);
	
	public function beforeFilter() {
		
		parent::beforeFilter();
		
		if ( $this->request->isPost() ) {
			$data = $this->request->data;
			if ( isset($data['SistemasCombo']) ) {
				// Alterando a variavel de Sessao que seleciona o Sistema
				$this->Session->write('SistemasCombo', $data['sistema_id']);
			}
		}
		
		if ( $this->Session->check('SistemasCombo') ) {
			$SistemasCombo = $this->Session->read('SistemasCombo');
		} else {
			$SistemasCombo = 0;
		}
		$this->set('SessionSistemasCombo', $SistemasCombo);
		$this->SistemasCombo = $SistemasCombo;

		// Carregar Layout bootstrap
		$this->layout = 'Admin.admin';
		$menus = $this->MenuAdmin->generate();
		$this->set('menus', $menus);
		$this->set('usuario', $this->Auth->user());
		$this->set('SistemasCombo', array('0'=>'Escolha o Sistema') + $this->Sistema->find('list', array('fields'=>array('id','nome'))));
		
		$this->set('listActions', $this->listActions);
		$this->set('indexActions', $this->indexActions);
		$this->set('formActions', $this->formActions);
		
	}

}

<?php

class CtrlComponent extends Component {

	public function getPlugins() {
		$allPlugins = App::objects('plugin');
		foreach($allPlugins as $key=>$plugin) {
			if ($plugin == 'Admin' or $plugin == 'Bootstrap' or $plugin == 'DebugKit') unset($allPlugins[$key]);
		}
		return $allPlugins;
	}
	
	public function getControllers($plugin) {
		$allControllers = App::objects($plugin.'.Controller');
		return $allControllers;
	}
	
	public function getActions($plugin, $controller) {
		App::import('Controller', str_replace('Controller', '', $plugin.'.'.$controller));
		$allActions = get_class_methods($controller);
		
		foreach ($allActions as $key=>$Action) {
			if ($Action{0} == '_') {
				unset($allActions[$key]);
			}
		}
		// Remove actions do AppController
		App::import('Controller','AppController');
		$parentActions = get_class_methods('AppController');
		$allActions = array_diff($allActions, $parentActions);

		// Remove actions do AppController do Plugin
		App::import('Controller',$plugin.'.'.$plugin.'AppController');
		$parentActions = get_class_methods('AppController');
		$allActions = array_diff($allActions, $parentActions);
		
		return $allActions;
	}

}

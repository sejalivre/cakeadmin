<?php
App::uses('Component', 'Controller');
class MenusComponent extends Component {
	var $uses = array('Admin.Link');
	var $components = array('Auth');
	
	public function generate($Menu, $instancia, $plugin) {
		$conditions = array(
			'InstanciaSistema.plugin' => $plugin,
			'InstanciaSistema.instancia' => $instancia
		);
		$InstanciaSistema = $Menu->Sistema->InstanciaSistema->find('first',array('conditions'=>$conditions));
		
		$sistema_id = $InstanciaSistema['Sistema']['id'];
		
		$Menu->Link->Behaviors->attach('Containable');
		$Menu->Link->contain(
			'Permissao',
			'Menu',
			'Menu.Sistema',
			'Menu.Sistema.InstanciaSistema'
		);
		$user = $this->Auth->user();
		$conditions = array(
			'Menu.grupo_id' => $user['grupo_id'],
			'Menu.sistema_id' => $sistema_id
		);
		$Menus = $Menu->Link->find('threaded',
			array(
				'conditions' => $conditions,
				'order' => array('Link.texto'=>'asc')
			));
		return $Menus;
	}
}

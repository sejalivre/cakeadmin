<?php
class PermissoesController extends AdminAppController {

	public $uses = array('Admin.Permissao');
	
	function _save($id = null, $grupo_id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			if ( $this->action == 'edit' ) {
				$data['Permissao']['id'] = $id;
			}
			$data['Permissao']['grupo_id'] = $grupo_id;
			
			if ($this->Permissao->save($data)) {
				$this->Bootstrap->setFlash('Registro salvo com sucesso!');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar o Registro!','warning');
			}
		}
	}
	
	function _related($Exclude = false) {
		$grupos = array();
		if ($Exclude) {
		foreach ($Exclude as $grupo) {
			array_push($grupos, intval($grupo['Grupo']['id']));
		}
		}
		$conditions = array(
			'NOT' => array(
				'Grupo.id' => $grupos
			)
		);
		$Grupos = $this->Permissao->GrupoPermissao->Grupo->find('list',array('fields'=>array('id','nome'),'conditions'=>$conditions));
		$this->set('Grupos',$Grupos);
	}
	
	public function index($grupo_id = null) {
		// Configura Titulo da Pagina
		$this->set('title_for_layout','Permissões');
		$conditions = array(
			'Permissao.grupo_id' => $grupo_id
		);
		// Carrega dados do BD
		$this->set('data', $this->Paginator->paginate('Permissao', $conditions));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Permissões');
		$this->set('panelStyle', 'primary');
	}
	
	public function addindex() {
	
		$this->set('title_for_layout','Permissões - Lista');
		
		$Permissoes = array();
		
		$Plugins = $this->Ctrl->getPlugins();
		foreach ($Plugins as $plugin) {
			$Permissoes[$plugin] = array();
			$Controllers = $this->Ctrl->getControllers($plugin);
			foreach ($Controllers as $controller) {
				$Permissoes[$plugin][$controller] = array();
				$Actions = $this->Ctrl->getActions($plugin,$controller);
				foreach ($Actions as $action) {
					array_push($Permissoes[$plugin][$controller], $action );
				}
			}
		}
		
		$this->set('data', $Permissoes);
	}
	
	public function add($grupo_id = null) {
	
		$this->set('title_for_layout','Permissões - Adicionar');

		$this->_save($id, $grupo_id);
		
		$this->_related();
		
		$this->set('pageHeader', 'Nova Permissão');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Permissao');

		
		$this->render('form');
	}
	
	public function edit($permissao_id = null) {
	
		$this->set('title_for_layout','Permissões - Editar');
		
		$this->_save($permissao_id, $grupo_id);
		$this->Permissao->Behaviors->attach('Containable');
		$this->Permissao->contain('GrupoPermissao','GrupoPermissao.Grupo');
		$Permissao = $this->Permissao->read(null, $permissao_id);
		$this->request->data = $Permissao;
		$this->set('permissao_id', $permissao_id);
				
		$this->_related($Permissao['GrupoPermissao']);

		$this->set('pageHeader', 'Edita Permissão');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Permissao');

		$this->render('form');
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			$Grupo = $this->Permissao->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com sucesso!');
			$this->redirect( array( 'action'=>'index' ));
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterPermissao')) {
			$this->request->data = $this->Session->read('filterPermissao');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterPermissao');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}
	public function delgrupo($grupopermissao_id = null, $permissao_id = null) {
		$this->layout = false;
		$this->Permissao->GrupoPermissao->delete($grupopermissao_id);
		$conditions = array(
			'GrupoPermissao.permissao_id' => $permissao_id
		);
		$related = $this->Permissao->GrupoPermissao->find('all',array('conditions'=>$conditions));
		$available = $this->Permissao->GrupoPermissao->Grupo->find('all');
		
		$data = array(
			$related,
			$available
		);
		$this->set('data', $data);
	}
	public function addgrupo($grupo_id = null, $permissao_id = null) {
		$this->layout = false;
		$data = array(
			'grupo_id' => $grupo_id,
			'permissao_id' => $permissao_id
		);
		$this->Permissao->GrupoPermissao->save($data);
		
		$conditions = array(
			'GrupoPermissao.permissao_id' => $permissao_id
		);
		$related = $this->Permissao->GrupoPermissao->find('all',array('conditions'=>$conditions));
		$available = $this->Permissao->GrupoPermissao->Grupo->find('all');
		$data1 = array(
			$related,
			$available
		);
		$this->set('data', $data1);
	}

}
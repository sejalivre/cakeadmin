<?php
class GruposController extends AdminAppController {

	public $uses = array('Admin.Grupo');
	
	public function _save($id = null) {
		$data = $this->request->data;
		if ($this->request->isPost()) {
			if ( $this->action == 'edit' ) {
				$data['Grupo']['id'] = $id;
			} else {
				$data['Grupo']['sistema_id'] = $this->SistemasCombo['id'];
			}
			if ( $this->Grupo->save( ( $data ) ) ) {
				$this->Bootstrap->setFlash('Registro salvo com successo!');
				$this->redirect( array( 'action'=>'index' ));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar Registro!');
			}
		}
	}
	
	public function _related() {
	}

	public function index() {
		// Configura Titulo da Pagina
		$this->set('title_for_layout','Grupo');
		$conditions = array(
			'Grupo.sistema_id' => $this->SistemasCombo
		);
		// Carrega dados do BD
		$this->set('data', $this->Paginator->paginate('Grupo', $conditions));

		$this->set('pagination', true);
		$this->set('pageHeader', 'Grupos');
		$this->set('panelStyle', 'primary');
	}
	
	public function add() {
		$this->_save();
		$this->_related();
		
		$this->set('pageHeader', 'Novo Grupo');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Grupo');
		
		$this->render('form');
	}
	public function edit($id = null) {
		$this->_save($id);
		$this->_related($id);
		
		$Grupo = $this->Grupo->read(null, $id);
		$this->request->data = $Grupo;

		$this->set('pageHeader', 'Edita Grupo');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Grupo');
		
		$this->render('form');
	}
	
	// Exclui registro
	public function del( $id = null ) {
		if ($this->request->isPost()) {
			try {
				$this->Grupo->delete($id);
				$this->Bootstrap->setFlash('Registro excluido com sucesso!.','info');
			} catch (Exception $e) {
				$this->Bootstrap->setFlash('Erro na exclusão do Registro! Em uso em relacionamento.','danger');
			}
		} else {
			$this->Bootstrap->setFlash('Erro na exclusão do Registro!','danger');
		}
		$this->redirect(array('action'=>'index'));
	}
}
<?php
class SistemasController extends AdminAppController {

	public $uses = array('Admin.Sistema');
	
	function save($id = null) {
		if ($this->request->isPost()) {
			if ( $this->action == 'edit' ) {
				$this->request->data['Sistema']['id'] = $id;
			}
			if ( $this->Sistema->save( ( $this->request->data ) ) ) {
				$this->Bootstrap->setFlash('Registro salvo com successo!');
				$this->redirect( array( 'action'=>'index' ));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar Registro!');
			}
		}
	}

	public function index() {
		// Configura Titulo da Pagina
		$this->set('title_for_layout','Sistemas');
		// Carrega dados do BD
		$this->set('data', $this->Paginator->paginate('Sistema'));
	}
	
	public function add() {
		$this->save();
		$this->render('form');
	}
	public function edit($id = null) {
		$this->save($id);
		if (!$id) {
			$this->Session->setFlash('Sistema não existente!');
			$this->redirect(array('action'=>'index')); 
		} else {
			$Sistema = $this->Sistema->read(null, $id);
			$this->request->data = $Sistema;
		}
		$this->render('form');
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			$Sistema = $this->Sistema->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com successo!');
			$this->redirect( array( 'action'=>'index' ));
		}
	}
}
<?php
class InstanciasController extends AdminAppController {

	public $uses = array('Admin.Instancia');
	
	function save($id = null) {
		if ($this->request->isPost()) {
			if ( $this->action == 'edit' ) {
				$this->request->data['Instancia']['id'] = $id;
			}
			if ( $this->Instancia->save( ( $this->request->data ) ) ) {
				$this->Bootstrap->setFlash('Registro salvo com successo!');
				$this->redirect( array( 'action'=>'index' ));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar Registro!');
			}
		}
	}

	public function index() {
		// Configura Titulo da Pagina
		$this->set('title_for_layout','Grupo');
		// Carrega dados do BD
		$this->set('data', $this->Paginator->paginate('Instancia'));
	}
	
	public function add() {
		$this->save();
		$this->render('form');
	}
	public function edit($id = null) {
		$this->save($id);
		if (!$id) {
			$this->Session->setFlash('Instância não existente!');
			$this->redirect(array('action'=>'index')); 
		} else {
			$Grupo = $this->Instancia->read(null, $id);
			$this->request->data = $Grupo;
		}
		$this->render('form');
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			$Grupo = $this->Instancia->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com successo!');
			$this->redirect( array( 'action'=>'index' ));
		}
	}
	
	public function sistemas($id = null) {
		$this->Instancia->Behaviors->attach('Containable');
		$this->Instancia->contain(
			'InstanciaSistema',
			'InstanciaSistema.Sistema'
		);
		$Instancia = $this->Instancia->read(null, $id);
		$this->set('data', $Instancia['InstanciaSistema']);
	}
}
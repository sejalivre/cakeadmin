<?php
class MenusController extends AdminAppController {

	public $uses = array('Admin.Menu');
	
	function _save($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			if ( $this->action == 'edit' ) {
				$data['Menu']['id'] = $id;
			} else {
				$data['Menu']['sistema_id'] = $this->SistemasCombo['id'];
			}
			if ($this->Menu->save($data)) {
				$this->Bootstrap->setFlash('Registro salvo com sucesso!');
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar o Registro!','warning');
			}
		}
	}
	
	function _related() {
		$Grupos = array('0'=>'Selecione') + $this->Menu->Grupo->find('list',array('fields'=>array('id','nome')));
		$this->set('Grupos',$Grupos);
	}
	
	public function index() {
	
		$this->set('title_for_layout','Menus - Lista');
		
		$this->Menu->Behaviors->attach('Containable');
		$this->Menu->contain('Grupo');
		$conditions = array(
			'Menu.sistema_id' => $this->SistemasCombo
		);
		$Menus = $this->Paginator->paginate('Menu', $conditions);
		$this->set('data', $Menus);
		$this->set('pagination', true);
		$this->set('pageHeader', 'Menus');
		$this->set('panelStyle', 'primary');
	}
	
	public function add() {
	
		$this->set('title_for_layout','Menus - Adicionar');

		$this->_save();
		
		$this->_related();
		
		$this->set('pageHeader', 'Novo Menu');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Menu');
		
		$this->render('form');
	}
	
	public function edit($menu_id = null) {
	
		$this->set('title_for_layout','Menus - Editar');
		
		$this->_save($menu_id);
	
		$Menu = $this->Menu->read(null, $menu_id);
		$this->request->data = $Menu;
		
		$this->_related();
		
		$this->set('pageHeader', 'Edita Menu');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Menu');

		$this->render('form');
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			$Grupo = $this->Menu->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com sucesso!');
			$this->redirect( array( 'action'=>'index' ));
		}
	}
	
	public function _linksRelated($grupo_id = null) {
		$conditions = array(
			'Permissao.grupo_id' => $grupo_id
		);
		$this->set('Permissoes', $this->Menu->Link->Permissao->find('list', array('fields'=>array('id','item'),'conditions'=>$conditions)));
		
		$conditions = array(
			'Link.parent_id IS NULL'
		);
		$this->set('Links', $this->Menu->Link->find('list', array('fields'=>array('id','texto'),'conditions'=>$conditions)));
	}
	
	public function links($menu_id = null) {
		$Menu = $this->Menu->read(null, $menu_id);
		$conditions = array(
			'menu_id' => $menu_id
		);
		$links = $this->Menu->Link->find('threaded', array('conditions'=>$conditions));
		$this->set('data', $links);
		$this->set('menu_id', $menu_id);
		$this->set('grupo_id', $Menu['Menu']['grupo_id']);
		//$this->set('pagination', true);
		$this->set('pageHeader', 'Links');
		$this->set('panelStyle', 'primary');
	}
	
	public function _linksSave($link_id = null, $menu_id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			if ( $link_id ) {
				$data['Link']['id'] = $id;
			}
			$data['Link']['menu_id'] = $menu_id;
			
			if ($this->Menu->Link->save($data)) {
				$this->Bootstrap->setFlash('Registro salvo com sucesso!');
				$this->redirect(array('action'=>'links', $menu_id));
			} else {
				$this->Bootstrap->setFlash('Erro ao salvar o Registro!','warning');
			}
		}
	}
	
	public function linksAdd($menu_id = null, $grupo_id = null) {
		$this->_linksSave(null, $menu_id);
		$this->_linksRelated($grupo_id);
		$this->set('pageHeader', 'Novo Link');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Link');
		
		$this->set('menu_id', $menu_id);
		$this->render('linksForm');
	}
	
	public function linksEdit($menu_id = null, $grupo_id = null, $link_id = null) {
		$this->_linksSave($link_id, $menu_id);
		$this->_linksRelated($grupo_id);
		$this->set('pageHeader', 'Edita Link');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Link');
		
		$this->set('menu_id', $menu_id);
		
		$Link = $this->Menu->Link->read(null, $link_id);
		$this->request->data = $Link;
		
		$this->render('linksForm');
	}
	
	public function linksDel($menu_id = null, $id = null ) {
	}
	

}